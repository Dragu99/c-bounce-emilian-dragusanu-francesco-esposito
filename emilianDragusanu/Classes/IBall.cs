﻿using System;

namespace Classes
{
	public interface IBall
	{
        Body getBody();

        bool isBig();

        bool isSmall();

        bool isDead();

        void setBig();

        void setSmall();

        void bigBall();

        void smallBall();

        void big();

        void small();

        void jump();

        void movement(float direction);

        bool isRolling();

        void setRespawnX(float x);

        void setRespawnY(float y);
    }
}
