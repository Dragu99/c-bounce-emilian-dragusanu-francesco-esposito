﻿using System;

namespace Classes
{
    public class BallImpl: IBall
    {

        private State currentState;

        private State size;

        private static const int X_POS = 128;

        private static const int Y_POS = 64;

        private float respawnX = X_POS;

        private float respawnY = Y_POS;

        private static const float SIZE = 110;

        private static const float SIZE2 = -10000;

        private static const float SIZE3 = -0.01;

        private static const float SIZE4 = 0.01;

        private static const float SMALLRADIUS = 11;

        private static const float BIGRADIUS = 17;

        private bool rolling;

        private bool jumping;

        private bool getBig;

        private bool getSmall;

        private Hud hud;

        private Bounce game;

        private PlayScreen playscreen;

        public BallImpl(PlayScreen playscreen, float x, float y, Bounce game)
        {
            base.(this.playscreen, x, y);
            this.rolling = false;
            this.GetBig = false;
            this.GetSmall = true;
            this.SetJumping(false);
            this.currentState = State.STANDING;
            this.size = State.SMALL;
            this.hud = this.playscreen.GetHud();
            this.SetGame(this.game);
            this.SetPlayscreen(this.playscreen);
        }


        public Body Body => this.body;


        public bool IsBig()
        {
            if (this.size != State.BIG)
            {
                return false;
            }
            return true;
        }

        public bool IsSmall()
        {
            if (this.size != State.SMALL)
            {
                return false;
            }
            return true;
        }

        public bool isDead => false;

        public void SetBig() => this.BigBall();

        public void SetSmall() => this.SmallBall();

        public void Big()
        {
            this.getBig = true;
            this.getSmall = false;
        }

        public void Small()
        {
            this.getSmall = true;
            this.getBig = false;
        }

        public bool IsRolling => this.rolling;

        public Vector2 Position => this.body.GetPosition();

        public State State => this.currentState;

        public void SetRespawnX(float x) => this.respawnX = x;

        public void SetRespawnY(float y) => this.respawnY = y;

        public PlayScreen Playscreen { get => this.playscreen; set => this.playscreen = value; }

        public Bounce Game { get => this.game; set => this.game = value; }

        public Hud GetHud() => this.hud; 

        public bool IsJumping()
        {
            return this.jumping;
        }

    }
}

