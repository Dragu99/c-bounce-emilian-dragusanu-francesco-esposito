﻿using System;

namespace Classes
{
    public class TypeDefImpl : ITypeDef
    {
        private Body body;

        private static const int RECTANGLE = 1;

        private static const int POLYGON = 2;

        private static const int BLOCK = 4;

        private static const int CHECKPOINT = 5;

        private static const int THORN = 6;

        private static const int RING = 7;

        private static const int LIFE = 8;

        private static const int FINISH_BLOCK = 9;

        private static const int HALF_BLOCK = 10;

        private static const int DEFLATER = 12;

        private static const int PUMPER = 11;

        private static const int END_BLOCK = 13;

        private Fixture fixture;

        public TypeDefImpl(PlayScreen playscreen, float x, float y, MapObject object)
        {
            base.(playscreen, x, y, object);
        }

        public void SetEntity(int shapeType, int obstacleId)
        {
            BodyDef bdf = new BodyDef();
            PolygonShape shape = new PolygonShape();
            FixtureDef fixDef = new FixtureDef();
            bdf.type = BodyDef.BodyType.StaticBody;
            if ((shapeType == RECTANGLE))
            {
                Rectangle rect = ((RectangleMapObject)(GetObject())).GetRectangle();
                bdf.position.set((rect.GetX()
                                + (rect.GetWidth() / 2)), (rect.GetY()
                                + (rect.GetHeight() / 2)));
                this.body = GetPlayScreen().GetWorld().CreateBody(bdf);
                shape.SetAsBox((rect.GetWidth() / 2), (rect.GetHeight() / 2));
                if ((obstacleId == BLOCK))
                {
                    fixDef.filter.categoryBits = Manager.BLOCK_ID;
                    fixDef.filter.maskBits = Manager.BOUNCE_ID;
                }
                else if ((obstacleId == THORN))
                {
                    fixDef.filter.categoryBits = Manager.THORN_ID;
                    fixDef.filter.maskBits = Manager.BOUNCE_ID;
                }
                else if ((obstacleId == RING))
                {
                    fixDef.filter.categoryBits = Manager.RING_ID;
                    fixDef.filter.maskBits = Manager.BOUNCE_ID;
                }
                else if ((obstacleId == FINISH_BLOCK))
                {
                    fixDef.filter.categoryBits = Manager.FINISH_BLOCK_ID;
                    fixDef.filter.maskBits = Manager.BOUNCE_ID;
                }
                else if ((obstacleId == DEFLATER))
                {
                    fixDef.filter.categoryBits = Manager.DEFLATER_ID;
                    fixDef.filter.maskBits = Manager.BOUNCE_ID;
                }
                else if ((obstacleId == PUMPER))
                {
                    fixDef.filter.categoryBits = Manager.PUMPER_ID;
                    fixDef.filter.maskBits = Manager.BOUNCE_ID;
                }
                else if ((obstacleId == END_BLOCK))
                {
                    fixDef.filter.categoryBits = Manager.END_BLOCK_ID;
                    fixDef.filter.maskBits = Manager.BOUNCE_ID;
                }

                fixDef.shape = shape;
                this.fixture = this.body.CreateFixture(fixDef);
            }
            else if ((shapeType == POLYGON))
            {
                Polygon polygon = ((PolygonMapObject)(GetObject())).GetPolygon();
                bdf.position.Set(polygon.GetX(), polygon.GetY());
                this.body = GetPlayScreen().GetWorld().CreateBody(bdf);
                shape.SetAsBox(polygon.GetScaleX(), polygon.GetScaleY());
                if ((obstacleId == CHECKPOINT))
                {
                    fixDef.filter.categoryBits = Manager.CHECKPOINT_ID;
                    fixDef.filter.maskBits = Manager.BOUNCE_ID;
                }
                else if ((obstacleId == LIFE))
                {
                    fixDef.filter.categoryBits = Manager.LIFE_ID;
                    fixDef.filter.maskBits = Manager.BOUNCE_ID;
                }
                else if ((obstacleId == HALF_BLOCK))
                {
                    fixDef.filter.categoryBits = Manager.HALFBLOCK;
                    fixDef.filter.maskBits = Manager.BOUNCE_ID;
                }

                fixDef.shape = shape;
                this.fixture = this.body.CreateFixture(fixDef);
            }

        }

        public Body GetBody() => this.body;

        public void SetCategoryFilter(int filterID)
        {
            Filter filter = new Filter();
            filter.categoryBits = ((short)(filterID));
            this.fixture.SetFilterData(filter);
        }

        public Fixture GetFixture() => this.fixture; 
    }
}

