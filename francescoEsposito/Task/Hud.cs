﻿using System;
namespace Task
{
    public class Hud
    { 
        private int ringCounter = 6;

        private int lifeCounter = 3;

      

        public Hud()
        {
        }

        public void LessLives()
        {
            this.lifeCounter--;
        }

        public void LessRings()
        {
            this.ringCounter--;
        }

        public int GetLife() => this.lifeCounter;

        public int GetRing() => this.ringCounter;
    }
}
