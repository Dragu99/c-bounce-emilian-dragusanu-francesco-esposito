﻿using System;

namespace Task
{
    public class BallViewImpl : Animations, BallView
    {

        private Body body;

        private static const int BOUNDS_WIDTH = 22;

        private static const int BOUNDS_HEIGHT = 22;

        private static const int BIG_BOUNDS_WIDTH = 34;

        private static const int BIG_BOUNDS_HEIGHT = 34;

        private State currentState;

        private State previousState;

        private static const int SIZE = 120;

        private TextureRegion ballStand;

        private Animation rollingBall;

        private float stateTimer;

        private Ball ball;

        private bool rollingRight;

        public BallViewImpl()
        {
            this.rollingRight = false;
            this.ballStand = new TextureRegion(atlas.findRegion("Animation_light_ball-1"), 0, 0, SIZE, SIZE);
            this.rollingBall = setAnimation(0, 4, "Animation_light_ball-1", SIZE, SIZE);
            setBounds(0, 0, BOUNDS_WIDTH, BOUNDS_HEIGHT);
        }

        private TextureRegion SetSprite(float delta)
        {
            TextureRegion region = new TextureRegion();
            region = this.getFrame(delta);
            if ((((this.body.getLinearVelocity().x < 0)
                        || !this.rollingRight)
                        && region.isFlipX()))
            {
                region.flip(true, false);
                this.rollingRight = true;
            }
            else if ((this.rollingRight && region.isFlipX()))
            {
                region.flip(true, false);
                this.rollingRight = false;
            }

            return region;
        }

        
        public override void SetBall(Ball ball)
        {
            this.ball = this.ball;
            this.body = this.ball.getBody();
        }

        
        public override float GetStateTimer()
        {
            return this.stateTimer;
        }

        
        public override void Update(float dt)
        {
            this.rollingRight = this.ball.isRolling();
            setPosition((this.body.getPosition().x
                            - (getWidth() / 2)), (this.body.getPosition().y
                            - (getHeight() / 2)));
            setRegion(this.setSprite(dt));
        }

        private TextureRegion GetFrame(float dt)
        {
            this.previousState = this.currentState;
            this.currentState = this.ball.getState();
            TextureRegion region = new TextureRegion();
            switch (this.currentState)
            {
                case ROLLING:
                    region = this.rollingBall.getKeyFrame(this.stateTimer, true);
                    break;
                case FALLING:
                case STANDING:
                    break;
                default:
                    region = this.ballStand;
                    break;
            }
            return region;
        }

   
        public override void Draw(Batch batch)
        {
            base.draw(batch);
        }
    }
}