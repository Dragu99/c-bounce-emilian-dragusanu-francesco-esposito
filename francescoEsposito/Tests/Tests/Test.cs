﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task;
using System;


namespace Tests
{
    [TestClass]
    public class Test
    {
        private Playscreen playscreen = new Playscreen();
        private Hud hud = new Hud();

        [TestMethod]
        public void TestPlayScreen()
        {
            Assert.IsNotNull(playscreen.VEL_ITERATION);
            Assert.IsNotNull(playscreen.POS_ITERATION);
            Assert.IsNotNull(playscreen.FREQUENCY);
            Assert.IsNotNull(playscreen.GetCam());
            Assert.IsNotNull(playscreen.GetMap());
            Assert.IsNotNull(playscreen.GetWorld());
            Assert.IsNotNull(playscreen.GetPlayer());
        }
        [TestMethod]
        public void TestHud()
        {
            Assert.IsNotNull(hud.GetRing());
            Assert.IsNotNull(hud.GetLife());
        }


    }
}
